﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ColorChanger : MonoBehaviour
{
    public Material blueMaterial = null;
    public Material purpleMaterial = null;

    private MeshRenderer meshRenderer = null;
    private XRGrabInteractable grabInteractable = null;
 
    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        grabInteractable = GetComponent<XRGrabInteractable>();

        grabInteractable.onActivate.AddListener(SetPurple);
        grabInteractable.onDeactivate.AddListener(SetBlue);
    }

    private void OnDestroy()
    {
        grabInteractable.onActivate.RemoveListener(SetPurple);
        grabInteractable.onDeactivate.RemoveListener(SetBlue);
    }

    private void SetPurple(XRBaseInteractor interactor)
    {
        meshRenderer.material = purpleMaterial;
    }

    private void SetBlue(XRBaseInteractor interactor)
    {
        meshRenderer.material = blueMaterial;
    }
}
