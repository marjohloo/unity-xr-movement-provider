﻿// Unity XR Movement Provider Script
//
// Setup:        https://www.youtube.com/watch?v=ndwJHpxd9Mo
// Movement:     https://www.youtube.com/playlist?list=PLmc6GPFDyfw87eECUxsoysoIz82VifRwt
// Gravity/Jump: https://answers.unity.com/questions/334708/gravity-with-character-controller.html
//
// ==============================================================================
//
// MIT License
//
// Copyright(c) 2020 Martin Looker
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class XrMovementProvider : LocomotionProvider
{
    public XRController controllerXZ;
    public XRController controllerY;

    public GameObject objectGrounded = null;
    public GameObject objectGravity = null;

    public Material materialTrue;
    public Material materialFalse;

    public float speedMove = 3.0f;
    public float speedJump = 6.0f;

    public bool applyGravity = true;
    
    private CharacterController characterController = null;
    private GameObject head = null;
    private Vector3 vectorMove;
    private float speedY = 0.0f;
    private bool primaryButton = false;
    private bool primary2DAxisClick = false;
    private MeshRenderer meshRendererGrounded = null;
    private MeshRenderer meshRendererGravity = null;

    protected override void Awake()
    {
        characterController = GetComponent<CharacterController>();
        head = GetComponent<XRRig>().cameraGameObject;
    }

    // Start is called before the first frame update
    void Start()
    {
        // Get indicator mesh renderers
        meshRendererGrounded = objectGrounded.GetComponent<MeshRenderer>();
        meshRendererGravity  = objectGravity.GetComponent<MeshRenderer>();
        // Position the character controller
        PositionController();   
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Zero movement at start of update
        vectorMove = Vector3.zero;
        // Position the character controller
        PositionController();
        // Grounded ?
        if (characterController.isGrounded)
        {
            // Update indicator 
            if (meshRendererGrounded != null && materialTrue != null) meshRendererGrounded.material = materialTrue;
            // Zero speedY
            speedY = 0;
        }
        // Not grounded ? 
        else
        {
            // Update indicator 
            if (meshRendererGrounded != null && materialFalse != null) meshRendererGrounded.material = materialFalse;
        }
        // Update movement based upon controller input
        CheckForInput();
        // Gravity applies ?
        if (applyGravity)
        {
            // Update indicator 
            if (meshRendererGravity != null && materialTrue != null) meshRendererGravity.material = materialTrue;
            // Apply gravity
            ApplyGravity();
        }
        // No gravity ? 
        else
        {
            // Update indicator 
            if (meshRendererGravity != null && materialFalse != null) meshRendererGravity.material = materialFalse;
            // Zero speedY
            speedY = 0;
        }
        // Move 
        characterController.Move(vectorMove * Time.deltaTime);
    }

    private void PositionController()
    {
        // Get the head in local, playspace ground
        float headHeight = Mathf.Clamp(head.transform.localPosition.y, 1, 2);
        characterController.height = headHeight;

        // Cut in half, add skin
        Vector3 newCenter = Vector3.zero;
        newCenter.y = characterController.height / 2;
        newCenter.y += characterController.skinWidth;

        // Move capsule in local space as well
        newCenter.x = head.transform.localPosition.x;
        newCenter.z = head.transform.localPosition.z;

        // Apply to character controller
        characterController.center = newCenter;
    }

    private void CheckForInput()
    {
        if(controllerXZ.enableInputActions)
        {
            CheckForMovementXZ(controllerXZ.inputDevice);
        }
        if (controllerY.enableInputActions)
        {
            CheckForMovementY(controllerY.inputDevice);
        }
    }

    private void CheckForMovementXZ(InputDevice device)
    {
        if(device.TryGetFeatureValue(CommonUsages.primary2DAxis, out Vector2 position))
        {
            StartMoveXZ(position);
        }
    }

    private void StartMoveXZ(Vector2 position)
    {
        // Apply the touch position to the heads forward vector
        Vector3 direction = new Vector3(position.x, 0, position.y);
        Vector3 headRotation = new Vector3(0, head.transform.eulerAngles.y, 0);

        // Rotate the input direction by the horizontal head rotation
        direction = Quaternion.Euler(headRotation) * direction;

        // Apply speed
        vectorMove = direction * speedMove;
    }

    private void CheckForMovementY(InputDevice device)
    {
        if (device.TryGetFeatureValue(CommonUsages.primary2DAxisClick, out bool click))
        {
            // Click just changed? 
            if (click != primary2DAxisClick)
            {
                // Store click 
                primary2DAxisClick = click;
                // Just pressed ?
                if (primary2DAxisClick == true)
                {
                    // Toggle gravity
                    applyGravity = !applyGravity;
                }
            }
        }
        // Applying gravity ? 
        if (applyGravity)
        {
            if (device.TryGetFeatureValue(CommonUsages.primaryButton, out bool button))
            {
                StartJumpY(button);
            }
        }
        else
        {
            if (device.TryGetFeatureValue(CommonUsages.primary2DAxis, out Vector2 position))
            {
                StartMoveY(position);
            }
        }
    }

    private void StartMoveY(Vector2 position)
    {
        // Apply the touch position to the heads forward vector
        Vector3 direction = new Vector3(0, position.y, 0);
//      Vector3 headRotation = new Vector3(0, head.transform.eulerAngles.y, 0);

        // Rotate the input direction by the horizontal head rotation
//        direction = Quaternion.Euler(headRotation) * direction;

        // Apply speed
        vectorMove.y = position.y * speedMove;
    }

    private void StartJumpY(bool button)
    {
        // Button is different ?
        if (button != primaryButton)
        {
            // Retain button state
            primaryButton = button;
            // Grounded and button pressed ? 
            if (characterController.isGrounded && primaryButton)
            {
                speedY += speedJump;
            }
        }
    }

    private void ApplyGravity()
    {
        // Update speedY for gravity
        speedY += Physics.gravity.y * Time.deltaTime;
        // Include speedY in move vector
        vectorMove.y = speedY;
    }
}
