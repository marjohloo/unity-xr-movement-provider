# unity-xr-movement-provider #

**Newer version at https://bitbucket.org/marjohloo/marjohloo-unity-xr-prototype which incorporates snap turn, jumping, running and flying, including run-time toggles for the last two. 
Public variables are available to toggle whether Run, Jump and Fly are allowed.**

This repository contains a Unity XR movement provider script with a basic scene to play in.

## Versions ##

Built on Unity 2019.3.13f1 using Oculus Rift.

## Sources ##

This code is based upon the following resources:

**[VR with Andrew - Unity XR Interaction Toolkit - Setup (on YouTube)](https://youtu.be/ndwJHpxd9Mo) **

Great video for getting set up.

**[VR with Andrew - Unity XR Toolkit Movement (on YouTube)](https://www.youtube.com/playlist?list=PLmc6GPFDyfw87eECUxsoysoIz82VifRwt) **

Video explaining basic movement, but gravity is not quite right in the on-screen code so...

**[Gravity with character controller (aldonaletto on Unity Answers)](https://answers.unity.com/questions/334708/gravity-with-character-controller.html) **

Explains how to apply gravity with a character controller, includes a jump function.

## Additions ##

Working from the above the `Assets\Scripts\XrMovementProvider.cs` brings the above together along with a toggle to turn gravity on and off and allow flying.

### Public Variables / Usage ###

![public-variables](Doc/public-variables.png)

* **System** - the XR Rig
* **Controller XZ** - the controller to use for movement on the XZ axes using the controller's Primary 2D Axis, relative to the headset's rotation.
* **Controller Y** - the controller to use for movement on the Y axis. The Primary 2D Axis Click is used to toggle gravity on and off. With gravity on the Primary Button performs a jump, with gravity off the Primary 2D Axis Y movement moves up and down (flying).
* **Object Grounded** - object used to indicate if the character controller is currently grounded, the left hand cube in this case.
* **Object Gravity** -  object used to indicate if gravity is on or off, the right hand sphere in the example.
* **Material True** - material to be applied to indicator object if true (green).
* **Material False** - material to be applied to indicator object if false (red).
* **Speed Move** - speed of movement when using Primary 2D Axis movement.
* **Speed Jump** - speed of jump when Primary Button is pressed on Controller Y.
* **Apply Gravity** - initial gravity state.

## Further Information ##

Works well with the standard Snap Turn Provider script making use of the Controller Y, though suggest halving the Activation Timeout to 0.25.

The interactable blue cube on the floor is pretty glitchy is held whilst moving despite attempting the tweaks/hacks suggested in [VR with Andrew - Unity XR Interaction Toolkit - Interactables](https://youtu.be/PnnHx-DVDLE). Maybe the turning the RigidBody off when held might help.

## Disclaimers ##

Whilst I'm an embedded C programmer for my day job I've only been learning Unity for 7 weeks and XR for a couple of days. I'm just watching and reading stuff on the web to figure things out, so these uses might be completely inappropriate. 

## License ##

This code is released under the MIT License, see [LICENSE.txt](LICENSE.txt) for details.